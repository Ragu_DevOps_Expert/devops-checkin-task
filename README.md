# DevOps Technical Check-In

This is a technical check-in for DevOps engineers.

- - -

# Orientation & Instruction

This repository contains a simple program written in Golang named `pinger`. This service reponds with `"hello world"` at the root path and can be configured to ping another server through the environment variables (see the file at `./cmd/pinger/config.go`). By default, running it will make it start a server and ping itself.

A basic `Makefile` is provided that allows you to:

- pull in dependencies - `make dep`
- builds the binaries - `make build`
- test runs - `make run`
- run tests - `make test`

You may modify the above recipes according to your needs/wants but avoid modifying those tagged with `# DO NOT CHANGE THIS` as we will be using those to validate your work.

## Pre-requisites

You will need the following installed:

- `go` to run the application (check with `go version`)
- `docker` for image building/publishing (check with `docker version`)
- `docker-compose` for environment provisioning (check with `docker-compose version`)
- `git` for source control (check with `git -v`)
- `make` for simple convenience scripts (check with `make -v`)

You will also need the following accounts:

- GitLab.com ([click here to register/login](https://gitlab.com/users/sign_in))

It's okay if you do not know Golang, the tasks can be done with little to no Golang knowledge.

## Directory structure

| Directory | Description |
| --- | --- |
| `/bin` | Contains binaries |
| `/cmd` | Contains source code for CLI interfaces |
| `/deployments` | Contains image files and manifests for deployments |
| `/docs` | Contains documentation |
| `/vendor` | Contains dependencies (use `make dep` to populate it) |

> ragur@Raghu-PC MINGW64 ~/devops-checkin-task (main)
> $ ll
> total 37
> -rw-r--r-- 1 ragur 197609  1101 Feb 18 15:32 LICENSE
> -rw-r--r-- 1 ragur 197609  1422 Feb 18 15:32 Makefile
> -rw-r--r-- 1 ragur 197609  4613 Feb 18 15:32 README.md
> drwxr-xr-x 1 ragur 197609     0 Feb 22 19:49 bin/
> drwxr-xr-x 1 ragur 197609     0 Feb 22 19:49 build/
> drwxr-xr-x 1 ragur 197609     0 Feb 22 19:49 cmd/
> drwxr-xr-x 1 ragur 197609     0 Feb 22 19:49 deployments/
> drwxr-xr-x 1 ragur 197609     0 Feb 22 19:49 docs/
> -rw-r--r-- 1 ragur 197609   111 Feb 18 15:32 go.mod
> -rw-r--r-- 1 ragur 197609 13123 Feb 18 15:32 go.sum
> drwxr-xr-x 1 ragur 197609     0 Feb 22 19:49 vendor/



## Task Overview

There are 4 tasks.

There is no time limit on this, try to complete as many tasks as possible (as little as one task at least), feel free to modify your setup up till the face-to-face interview.

## Get Started

1. Clone this repository
2. Create your own repository on GitLab
3. Set your local repository's remote to point to your GitLab repository
4. Make your changes locally according to the tasks below
5. Push to your GitLab repository

ragur@Raghu-PC MINGW64 ~/devops-checkin-task (main)
$ ll
total 37
-rw-r--r-- 1 ragur 197609  1101 Feb 18 15:32 LICENSE
-rw-r--r-- 1 ragur 197609  1422 Feb 18 15:32 Makefile
-rw-r--r-- 1 ragur 197609  4613 Feb 18 15:32 README.md
drwxr-xr-x 1 ragur 197609     0 Feb 22 19:49 bin/
drwxr-xr-x 1 ragur 197609     0 Feb 22 19:49 build/
drwxr-xr-x 1 ragur 197609     0 Feb 22 19:49 cmd/
drwxr-xr-x 1 ragur 197609     0 Feb 22 19:49 deployments/
drwxr-xr-x 1 ragur 197609     0 Feb 22 19:49 docs/
-rw-r--r-- 1 ragur 197609   111 Feb 18 15:32 go.mod
-rw-r--r-- 1 ragur 197609 13123 Feb 18 15:32 go.sum
drwxr-xr-x 1 ragur 197609     0 Feb 22 19:49 vendor/


- - -

# 1. Containerisation

## Context

Let's create a container with executable go application.

## Task

Create a `Dockerfile` in the `./deployments/build` directory according to any best practices you may know about. You may write tests as you see fit.

## Deliverable

Running `docker build -f ./deployments/build/Dockerfile -t devops/pinger:latest .` 
should result in a successful image named `devops/pinger:latest` which is reflected in the output of `docker image ls`.

Running `docker run -it -p 8000:8000 devops/pinger:latest` should result in the same behaviour as running `go run ./cmd/pinger`.

You can test if this works by running:

```sh
# to test the build
make docker_image;

# to test the runtime
make docker_testrun;
```

## Deliverable

`.gitlab-ci.yml` in the root of this directory that results in a successful build on your own repository with the required artifacts available for download.

- - -

# 2. Environment

## Context

Let's put a use case to it and demonstrate how it may be used downstream the value chain!

## Task

Create a `docker-compose.yml` in the `./deployments` to demonstrate two `pinger` services that ping each other

## 3. Deliverable

Running `docker-compose up -f ./deployments/docker-compose.yml` should result in a network of at least 2 Docker containers that are pinging each other 
with the other acting as an echo server. Exposing the logs should reveal them pinging each other at their different ports.

You can test if this works by running:

```sh
make testenv;
```

- - -

# 4. Pipeline

## Context

Automation is key in DevOps to deliver value continuously and the first step we can take for this poor un-automated repository is
to create a sensible pipeline that automates the build/test/release process. Since we might not be pushing to a Docker registry,
save the created Docker image into a tarball (see `docker_tar` and `docker_untar` in the Makefile for more info!).

## Task

Create a pipeline that results in:

1. The binary being built
2. Docker image being built

The following should also be exposed as GitLab job artifacts:

1. The binary itself
2. Docker image in `.tar` format

---

# Done?

Send the link to your GitLab repository to the person who requested you to engage in this check-in.

- - -
